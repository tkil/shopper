import { Component, OnInit } from '@angular/core';
import { ShopperService } from './shared/services/shopper.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'shopper';
  products = [];

  constructor(private shopperService: ShopperService) {

  }

  ngOnInit(): void {
    this.getProducts();
  }

  removeProduct(product: string) {
    this.shopperService.deleteProduct(product);
    this.getProducts();
  }

  addProduct(product: string) {
    if (product) {
      this.shopperService.addProduct(product);
      this.getProducts();
    }
  }

  getProducts() {
    return this.products = this.shopperService.getAllProducts();
  }
}
