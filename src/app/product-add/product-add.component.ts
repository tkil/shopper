import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent {
  @Output() onProductAdd = new EventEmitter();
  product = ""

  onProductSubmit(product) {
    this.onProductAdd.emit(product)
    this.product = "";
  }
}
