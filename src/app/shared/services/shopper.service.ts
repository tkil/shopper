import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShopperService {
  products = ["foo", "bar", "bizz", "bazz", "egg" , window.location.href];
  
  constructor() { }

  getAllProducts() {
    return this.products.sort();
  }

  addProduct(product: string) {
    this.products = [...this.products, product];
  }

  deleteProduct(product: string) {
    this.products = this.products.filter((p) => p !== product)
  }
}
